var mongoose 	= require('mongoose');
var Schema = mongoose.Schema;

var imageSchema = new Schema({
					name: {type: String,required: true},
					shortid: {type: String,required: true},
					tags: [String],
					expressions: [String]
}, {timestamps: true});

module.exports 	= mongoose.model('images', imageSchema);
