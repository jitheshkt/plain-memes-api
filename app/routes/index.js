var express = require('express');
var router = express.Router();
var imageController = require('../controllers/image');
var searchController = require('../controllers/search');
var tagController = require('../controllers/tag');

router.get('/', function(req, res) {
  res.json({
    message: 'Server is up and running.',
    version : '1.0'
  });
});

router.post('/image/upload', imageController.uploadImage);
router.get('/download/:imageid', imageController.singleImage);
router.get('/search', searchController.searchImage);
router.get('/tags', tagController.randomTags);

module.exports = router;
