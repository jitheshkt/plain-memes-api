var imageModel = require('../models/image');


exports.searchImage = function (req, res) {
  var keyword = req.query.s.toLowerCase(); //Reffer 3305561, 26699885
  imageModel.find({
    tags: {
      $regex: keyword
    }
  }, function (err, images) {
    if (err) {
      console.log(err);
      res.status(500).json({
        status: false,
        message: 'internal error occured during search'
      });
    } else {
      if (images.length > 0) {
        var payload = [];
        Object.keys(images).forEach(function (key) {
          payload.push({
            id: images[key].shortid,
            imageurl: 'http://' + req.headers.host + '/api/download/' + images[key].shortid,
            tags: images[key].tags,
            expressions: images[key].expressions,
            since: images[key].createdAt
          });
        });
        res.status(200).json({
          status: true,
          payload: payload
        });
      } else {
        res.status(204).json({
          status: true,
          payload: []
        });
      }
    }
  });
};