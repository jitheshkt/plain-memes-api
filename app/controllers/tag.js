var imageModel = require('../models/image');

exports.randomTags = function(req, res) {
imageModel.find({}, 'tags', function(err, tags){
    if(err) {
        res.status(500).json({
            status: false,
            message: 'internal error occured during search'
          });
    } else {
        var tagsObject = [];
        Object.keys(tags).forEach(function(key){
            tagsObject = tagsObject.concat.apply(tagsObject, tags[key].tags);
        });
        res.status(200).json({
            status : true,
            payload : Array.from(new Set(tagsObject))
        });
    }
});
};