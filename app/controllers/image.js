var multer = require('multer');
var crypto = require('crypto');
var path = require('path');
var shortid = require('shortid');

var config = require('../../config');
var imageModel = require('../models/image');

shortid.characters(config.userkeyStrings);


const storage = multer.diskStorage({
  destination: 'uploads/images/',
  filename: function (req, file, callback) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      if (err) return callback(err);
      callback(null, raw.toString('hex') + path.extname(file.originalname));
    });
  }
});

exports.uploadImage = function (req, res) {
    var upload = multer({
      storage: storage
    }).single('image');
    upload(req, res, function (err) {
      if (err) {
        console.log(err);
        res.status(500).json({
          status: false,
          message: 'internal error occured'
        });
      } else {
        console.log(req.body);
        console.log(req.file);
        if(req.file || !req.body.tags || !req.body.expressions) {
          var newImage = new imageModel({
            name: req.file.filename,
            shortid: shortid.generate(),
            tags: req.body.tags,
            expressions: req.body.expressions
          });
          newImage.save(function (err) {
            if (err) {
              res.status(500).json({
                status: false,
                message: 'internal error occured'
              });
            } else {
              res.status(200).json({
                status: true,
                message: 'image added'
              });
            }
          });
        } else {
          res.status(406).json({
            status: false,
            message: 'tags, expressions, image can not be empty'
          });
        }
      }
    });
};

exports.singleImage = function (req, res) {
  var imageid = req.params.imageid;
  imageModel.findOne({
    shortid: imageid
  }, function (err, image) {
    if (err) {
      res.status(500).json({
        status: false,
        message: 'internal error occured'
      });
    } else {
      res.sendFile(image.name, {
        root: path.join(__dirname, '../../uploads/images')
      });
    }
  });
};