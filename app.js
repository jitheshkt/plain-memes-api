var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var router = require('./app/routes/index');
var config = require('./config');
var path   = require('path');
var port = 3000;

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header('Access-Control-Allow-Methods', "GET,PUT,POST,DELETE,PATCH,OPTIONS");
  next();
});

mongoose.connect(config.database);
app.set('appSecret', config.secret);


app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(bodyParser.json());

app.use(morgan('dev'));

app.use('/api', router);

app.listen(port);
console.log("API started and listening at " + port);
